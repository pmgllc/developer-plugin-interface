=== Remove Comment Notes ===
Contributors: peterdog
Website Link: http://www.petersenmediagroup.com/plugins/
Donate link: https://www.petersenmediagroup.com/contribute
Tags: api keys, secret keys
Requires at least: 3.0
Tested up to: 3.6
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Adds functionality plugin via a plugin shell.

== Description ==

Adds functionality plugin via a plugin shell. Give your functions.php a rest. Design goes in functions.php. Functionality goes in this plugin.


== Installation ==

This section describes how to install the plugin and get it working.

1. Upload `remove-comment-notes` to the `/wp-content/plugins/` directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. That's it.


== Frequently Asked Questions ==


= How does it work? =

Activate the plugin and the notes field *should* disappear.

= Where is the settings menu? =

There isn't one. On purpose.

= They still show up on my theme. =

Then you're using a theme / framework that has gone cowboy and done it their own way. I can't do anything about that.


== Screenshots ==

1. Before
2. After



== Changelog ==

= 1.0 =
* First release!


== Upgrade Notice ==

= 1.0 =
* First release!
